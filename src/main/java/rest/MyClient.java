package rest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import model.City;
import model.CityManager;
import model.CityManagerService;
import model.Position;
public class MyClient {
	private static final QName SERVICE_NAME =
            new QName("http://model/", "CityManagerService");
    private static final QName PORT_NAME =
            new QName("http://model/", "CityManagerPort");

    public static void main(String args[]) throws MalformedURLException {
        URL wsdlURL = new URL("http://127.0.0.1:808 4/citymanager?wsdl");
        Service service = Service.create(wsdlURL, SERVICE_NAME);
        CityManagerService cityManager = service.getPort(
                PORT_NAME, CityManagerService.class);
        System.out.println(cityManager.getCities());
        City c = new City("Zanarkand", 16, 64, "Hyrule");
        cityManager.addCity(c);
        cityManager.removeCity(c);
        System.out.println(cityManager.getCities());
    }
	/*public static void main(String args[]) throws Exception {
		MyClient client = new MyClient();
		client.searchForCities("all");
        client.requestDelete(null);
        client.searchForCities("all");
        client.addCity(new City("Rouen", 49.443889, 1.103333, "France"));
        client.addCity(new City("Mogadiscio", 2.333333, 48.85, "Somalie"));
        client.addCity(new City("Rouen", 49.443889, 1.103333, "France"));
        client.addCity(new City("Bihorel", 49.455278, 1.116944, "France"));
        client.addCity(new City("Londres", 51.504872, -0.07857, "Angleterre"));
        client.addCity(new City("Paris", 48.856578, 2.351828, "France"));
        client.addCity(new City("Paris", 43.2, -80.38333, "Canada"));
        client.searchForCities("all");
        client.addCity(new City("Villers-Boccage", 49.083333, -0.65, "France"));
        client.addCity(new City("Villers-Boccage", 50.021858, 2.326126, "France"));
        client.searchForCities("all");
        client.requestDelete(new City("Villers-Boccage", 49.083333, -0.65, "France"));
        client.searchForCities("all");
        client.requestDelete(new City("Londres", 51.504872, -0.07857, "Angleterre"));
        client.requestDelete(new City("Londres", 51.504872, -0.07857, "Angleterre"));
        client.searchForCity(new Position(49.443889, 1.103333));
        client.searchForCity(new Position(49.083333, -0.65));
        client.searchForCity(new Position(43.2, -80.38));
        client.searchNearForCity(new Position(48.85, 2.34));
        client.searchNearForCity(new Position(42, 64));
        client.searchNearForCity(new Position(49.45, 1.11));
        client.searchForCities("Mogadiscio");
        client.searchForCities("Paris");
        client.searchForCities("Hyrule");
        client.requestDelete(null);
        client.searchForCities("all");

	}*/
}
