package model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CityNotFound extends Exception {
    public CityNotFound(String errorMessage) {
        super(errorMessage);
    }
}


