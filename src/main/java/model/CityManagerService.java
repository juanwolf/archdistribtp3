package model;

import javax.jws.WebService;
import java.util.List;

/**
 * Created by juanwolf on 14/03/14.
 */
@WebService
public interface CityManagerService {
    boolean addCity(City city);
    boolean removeCity(City city);
    List<City> getCities();
    public void setCities(List<City> cities);
    public List<City> searchFor(String cityName);
    public City searchExactPosition(Position position) throws CityNotFound;
    public City searchNear(Position position) throws CityNotFound;
    public CityManager searchCitiesNear(Position position) throws CityNotFound;


}
