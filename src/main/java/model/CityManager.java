package model;

import java.util.LinkedList;
import java.util.List;
import java.lang.Math;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class represent a city manager, it can  
 * <ul>
 * 	<li>add a city</li>
 * 	<li>remove a city</li>
 * 	<li>return the list of cities</li>	
 * 	<li>search a city with a given name</li>
 *  <li>search a city at a position</li>
 * 	<li>return the list of cities near 10km of the given position</li>
 * </ul>
 *
 */
@WebService(endpointInterface = "model.CityManagerService",
        serviceName = "CityManagerService")
public class CityManager implements CityManagerService {
    /**
     * Default Distance for the functions 'near'.
     */
    private final static int PREFERRED_DISTANCE_NEAR = 10;
	private List<City> cities;
	
	public CityManager() {
		this.cities = new LinkedList<City>();
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	
	public boolean addCity(City city){
		return cities.add(city);
	}

    /**
     * Remove a city if it's in the CityManager else throw CityNotFound Exception.
     * @param city The city to remove.
     * @throws CityNotFound
     */
	public boolean removeCity(City city) {
        return cities.remove(city);
	}
	
	public List<City> searchFor(String cityName){
		List<City> result = new LinkedList<City>();
		for(City c : cities) {
			if (c.getName().equals(cityName)) {
				result.add(c);
			}
		}
		return result;
	}

    /**
     * Return the city with the Position equal at position.
     * @param position the position asked in WGS84.
     * @throws CityNotFound
     */
	public City searchExactPosition(Position position) throws CityNotFound {
		for(City city:cities){
			if (position.equals(city.getPosition())){
				return city;
			}
		}
		throw new CityNotFound("No city at the position: "
                + position.toString());
	}

    /**
     * Return a city near of (with a distance equal at PREFERRED_DISTANCE_NEAR)
     * the position.
     * @param position the position asked in WGS84.
     * @return
     * @throws CityNotFound
     */
    public City searchNear(Position position) throws CityNotFound {
        for(City city: cities) {
            double distance = Math.acos(
                    Math.sin(city.getPosition().getLatitude()) * Math.sin(position.getLatitude())
                            + Math.cos(city.getPosition().getLatitude()) * Math.cos(position.getLatitude())
                            * Math.cos(city.getPosition().getLatitude() - position.getLongitude())
            );
            if (distance < PREFERRED_DISTANCE_NEAR) {
                return city;
            }
        }
        throw new CityNotFound("No city around " + PREFERRED_DISTANCE_NEAR
                        + " km at the position " + position.toString());
    }

    /**
     * Return the cityManager with all the cities near of the position with a
     * distance less than PREFERRED_DISTANCE_NEAR.
     * @param position the position asked in WGS84.
     * @throws CityNotFound
     */
    public CityManager searchCitiesNear(Position position) throws CityNotFound {
        CityManager cityManager = new CityManager();
        for(City city: cities) {
            double distance = Math.acos(
                    Math.sin(Math.toRadians(city.getPosition().getLatitude()))
                            * Math.sin(Math.toRadians(position.getLatitude()))
                            + Math.cos(Math.toRadians(city.getPosition().getLatitude()))
                            * Math.cos(Math.toRadians(position.getLatitude())))
                            * 6371;
            if (distance < PREFERRED_DISTANCE_NEAR) {
                cityManager.addCity(city);
            }
        }
        if (cityManager.getCities().size() == 0) {
            throw new CityNotFound("No city around " + PREFERRED_DISTANCE_NEAR
                    + " km at the position " + position.toString());
        }
        return cityManager;
    }
}
